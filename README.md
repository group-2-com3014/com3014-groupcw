This repo contains all our code for the COM3014 group coursework.
## Getting Started

 

To get a local copy up and running follow these simple example steps.

 

### Prerequisites
- Docker

 


### Installation

 


1. Clone the repo
   ```sh
   git clone https://gitlab.eps.surrey.ac.uk/group-2-com3014/com3014-groupcw
   ```
2. Populate the 4 environment files for the posts-service/, comments-service/, users-service/, and apollo-gateway/ with the necessary details
3. Run docker compose
   ```sh
   docker compose up
   ```
4. The webiste should be accessible at
   ```
   http://localhost:3000
   ```
