import { AuthenticationError } from 'apollo-server-express';
import jwt from 'jsonwebtoken';

const auth = (context) => {
  const authHeader = context.req.headers.authorization;
  if (authHeader) {
    const token = authHeader.split('Bearer ')[1];
    if (token) {
      try {
        const user = jwt.verify(token, process.env.SECRET_KEY);
        return user;
      } catch (err) {
        throw new AuthenticationError('Invalid token');
      }
    }
    throw new Error("Authentication token must be of type 'Bearer [token]");
  }
  throw new Error('Authorization header not present');
};

export default auth;