import axios from 'axios';

const COMMENTS_SERVICE_URI = "http://comments-service:5003";

export default class PostsService {
  static async fetchAllPostComments({ postId }) {
    const { data } = await axios.get(`${COMMENTS_SERVICE_URI}/comments/post/${postId}`);
    return data;
  }

  static async getCommentById({ id }) {
    const { data } = await axios.get(`${COMMENTS_SERVICE_URI}/comments/${id}`);
    return data;
  }

  static async deleteCommentById({ id }) {
    await axios.delete(`${COMMENTS_SERVICE_URI}/comments/${id}`);
  }

  static async createComment(comment) {
    const { data } = await axios.post(`${COMMENTS_SERVICE_URI}/comments/`, comment);
    return data;
  }

  static async deleteAllPostComments({ postId }) {
    await axios.delete(`${COMMENTS_SERVICE_URI}/comments/post/${postId}`);
  }
}