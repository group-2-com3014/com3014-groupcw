import axios from 'axios';

const POSTS_SERVICE_URI = "http://posts-service:5002";

export default class PostsService {
  static async fetchAllPosts() {
    const { data } = await axios.get(`${POSTS_SERVICE_URI}/posts/`);
    return data;
  }

  static async getPostById({ id }) {
    const { data } = await axios.get(`${POSTS_SERVICE_URI}/posts/${id}`);
    return data;
  }

  static async createPost(post) {
    const { data } = await axios.post(`${POSTS_SERVICE_URI}/posts/`, post);
    return data;
  }

  static async deletePost({ id }) {
    await axios.delete(`${POSTS_SERVICE_URI}/posts/${id}`);
  }

  static async likePost({ postId, userId }) {
    await axios.patch(`${POSTS_SERVICE_URI}/posts/${postId}/like/${userId}`)
  }

  static async addCommentId({ commentId, postId }) {
    const { data } = await axios.patch(`${POSTS_SERVICE_URI}/posts/${postId}/comment/${commentId}`);
    return data;
  }

  static async deleteCommentId({ commentId, postId }) {
    const { data } = await axios.delete(`${POSTS_SERVICE_URI}/posts/${postId}/comment/${commentId}`);
    return data;
  }
}