import axios from 'axios';

const USERS_SERVICE_URI = "http://users-service:5001/user";

export default class UsersService {
  static async signup({ name, email, password }) {
    const { data } = await axios.post(`${USERS_SERVICE_URI}/signup`, {
      name,
      email,
      password
    });
    return data;
  }

  static async signin({ email, password }) {
    const { data } = await axios.post(`${USERS_SERVICE_URI}/signin`, {
      email,
      password
    });
    return data;
  }

  static async checkUserEmail({ email }) {
    const { data } = await axios.get(`${USERS_SERVICE_URI}/user/check/${email}`);
    return data;
  }

  static async getUserById({ id }) {
    const { data } = await axios.get(`${USERS_SERVICE_URI}/user/${id}`);
    return data;
  }
}