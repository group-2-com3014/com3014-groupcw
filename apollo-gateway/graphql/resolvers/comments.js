import { UserInputError } from 'apollo-server-express';
import PostsService from '../adapters/PostsService.js';
import CommentsService from '../adapters/CommentsService.js';
import UsersService from '../adapters/UsersService.js';
import auth from '../../auth/auth.js';

const commentsResolvers = {
  Mutation: {
    createComment: async (_, { body, postId }, context) => {
      const { _id } = auth(context);
      const post = await PostsService.getPostById({ id: postId });

      if (post) {
        const comment = await CommentsService.createComment({ body, postId, creatorId: _id });
        post = await PostsService.addCommentId({ commentId: comment._id, postId });
        return post;
      } else throw new UserInputError( `Post with id ${postId} not found`);
    },
    async deleteComment(_, { commentId, postId }, context) {
      const post = await PostsService.getPostById({ id: postId });

      if (post) {
        await CommentsService.deleteCommentById({ id: commentId });
        post = await PostsService.deleteCommentId({commentId, postId});
        return post;
      } else {
        throw new UserInputError(`Post with id ${postId} not found`);
      }
    }
  },
  Comment: {
    async creator(parent) {
      return await UsersService.getUserById({ id: parent.creatorId });
    }
  }
};

export default commentsResolvers;