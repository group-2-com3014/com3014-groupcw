import usersResolvers from './users.js';
import postsResolvers from './posts.js';
import commentsResolvers from './comments.js';

const resolvers = {
  Query: {
    ...postsResolvers.Query
  },
  Mutation: {
    ...usersResolvers.Mutation,
    ...postsResolvers.Mutation,
    ...commentsResolvers.Mutation
  },
  Subscription: {
    ...postsResolvers.Subscription
  },
  Post: {
    ...postsResolvers.Post
  },
  Comment: {
    ...commentsResolvers.Comment
  }
};

export default resolvers;