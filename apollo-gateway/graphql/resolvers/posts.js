import { AuthenticationError, UserInputError } from 'apollo-server-express';
import auth from '../../auth/auth.js';
import PostsService from '../adapters/PostsService.js';
import CommentsService from '../adapters/CommentsService.js';
import UsersService from '../adapters/UsersService.js'

const postResolvers = {
  Query: {
    async getPosts() {
      try {
        const posts = await PostsService.fetchAllPosts();
        return posts;
      } catch (err) {
        throw new Error(err);
      }
    },
    async getPost(_, { postId }) {
      try {
        const post = await PostsService.getPostById({ id: postId });
        if (post) {
          return post;
        } else {
          throw new Error(`Post with id ${postId} not found`);
        }
      } catch (err) {
        throw new Error(err);
      }
    }
  },
  Mutation: {
    async createPost(_, { title, description, image }, context) {
      const user = auth(context);
      const newPost = {
        title,
        description,
        image,
        creatorId: user._id,
      };
      const post = await PostsService.createPost(newPost);

      context.pubsub.publish('NEW_POST', {
        newPost: post
      });

      return post;
    },
    async deletePost(_, { postId }, context) {
      const user = auth(context);

      try {
        const post = await PostsService.getPostById({ id: postId });
        if (user._id === post.creatorId) {
          await PostsService.deletePost({ id: postId });
          await CommentsService.deleteAllPostComments({ postId });

          return 'Post deleted successfully';
        } else {
          throw new AuthenticationError('Authentication error');
        }
      } catch (err) {
        throw new Error(err);
      }
    },
    async likePost(_, { postId }, context) {
      const { _id } = auth(context);

      const post = await PostsService.getPostById({ id: postId });
      if (post) {
        post = await PostsService.likePost({postId, userId: _id})
        return post;
      } else throw new UserInputError(`Post with id ${postId} not found`);
    }
  },
  Subscription: {
    newPost: {
      subscribe: (_, __, { pubsub }) => pubsub.asyncIterator('NEW_POST')
    }
  },
  Post: {
    async creator(parent) {
      return await UsersService.getUserById({ id: parent.creatorId });
    },
    async comments(parent) {
      return await CommentsService.fetchAllPostComments({ postId: parent._id })
    }
  }
};

export default postResolvers;