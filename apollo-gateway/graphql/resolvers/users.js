import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { UserInputError } from 'apollo-server-express';
import UsersService from '../adapters/UsersService.js';

function generateToken(user) {
  return jwt.sign({ _id: user._id, email: user.email }, process.env.SECRET_KEY, { expiresIn: '1h' });
}

const usersResolvers = {
  Mutation: {
    async signin(_, { email, password }) {
      const user = await UsersService.checkUserEmail({ email });

      if (!user) {
        throw new UserInputError('User not found');
      }
      const match = await bcrypt.compare(password, user.password);
      if (!match) {
        throw new UserInputError('Incorrect details');
      }

      const token = generateToken(user);
      user['token'] = token

      return user;
    },
    async signup(_,{ name, email, password }) {
      const user = await UsersService.checkUserEmail({ email });
      if (user) {
        throw new UserInputError('Email already exists');
      }

      const hashedPassword = await bcrypt.hash(password, 12);
      const newUser = await UsersService.signup({ name, email, password: hashedPassword })
      const token = generateToken(newUser);
      newUser['token'] = token

      return newUser;
    }
  }
};

export default usersResolvers;