import { gql } from 'apollo-server-express';

const typeDefs = gql`
  type User {
    _id: ID!
    email: String!
    token: String!
    name: String!
  }

  type Post {
    _id: ID!
    title: String!
    description: String!
    creator: User!
    image: String!
    likes: [String]!
    comments: [Comment]!
    createdAt: String!
  }

  type Comment {
    _id: ID!
    body: String!
    creator: User!
    postId: String!
    createdAt: String!
  }
  
  type Query {
    getPosts: [Post]
    getPost(postId: ID!): Post
  }

  type Mutation {
    signup(name: String!, email: String!, password: String!): User!
    signin(email: String!, password: String!): User!
    createPost(title: String!, description: String!, image: String!): Post!
    deletePost(postId: ID!): String!
    createComment(body: String!, postId: ID!): Post!
    deleteComment(commentId: ID!, postId: ID!): Post!
    likePost(postId: ID!): Post!
  }
  type Subscription {
    newPost: Post!
  }
`;

export default typeDefs;