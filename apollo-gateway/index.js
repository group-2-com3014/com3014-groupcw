import { ApolloServer, PubSub } from 'apollo-server-express';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import typeDefs from './graphql/typeDefs.js';
import resolvers from './graphql/resolvers/index.js'
import dotenv from 'dotenv';

dotenv.config();

const app = express();
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());

const pubsub = new PubSub();
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => ({ req, pubsub })
});

server.applyMiddleware({ app, path: "/graphql" });

app.listen({ port: 5000 }, () => console.log("Apollo Server running on port 5000"));