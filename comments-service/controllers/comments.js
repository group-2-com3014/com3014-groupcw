import mongoose from 'mongoose';
import Comment from '../models/Comment.js';

export const getComments = async (req, res) => {
  try {
    const result = await Comment.find();
    return res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: "Something went wrong" });
  }
};

export const getPostComments = async (req, res) => {
  const { postId } = req.params;

  try {
    const result = await Comment.find({ postId });
    return res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: "Something went wrong" });
  }
}

export const deletePostComments = async (req, res) => {
  const { postId } = req.params;

  const result = await Comment.find({ postId });
  if (result) {
    await Comment.deleteMany({ postId });
  } else {
    return res.status(200);
  }

  res.json({ message: "Comments deleted successfully." });
}

export const createComment = async (req, res) => {
  const comment = req.body;
  const newComment = new Comment({ ...comment, createdAt: new Date().toISOString() });

  try {
      await newComment.save();
      return res.status(201).json(newComment);
  } catch (error) {
      return res.status(409).json({ message: error.message });
  }
}

export const getCommentById = async (req, res) => {
  const { id } = req.params;

  try {
    const result = await Comment.findById(id);

    return res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: "Something went wrong" });
  }
}

export const deleteCommentById = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No comment with id: ${id}`);

  try {
    await Comment.findByIdAndRemove(id);
    return res.status(200).json({ message: "Comment deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: "Something went wrong" });
  }
}
