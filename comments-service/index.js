import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';

import commentRoutes from './routes/comments.js';

dotenv.config();

const app = express();
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());

app.use("/comments", commentRoutes);

const PORT = 5003;

if (process.env.NODE_ENV !== "test") {
  mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => app.listen(PORT, "0.0.0.0", () => console.log(`Comments service running on ${PORT}...`)))
  .catch((error) => console.log(error.message));

  mongoose.set("useFindAndModify", false);
}

export default app;