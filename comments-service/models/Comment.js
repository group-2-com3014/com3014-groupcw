import mongoose from 'mongoose';

const commentSchema = mongoose.Schema({
  body: String,
  creatorId: String,
  postId: String,
  createdAt: {
    type: Date,
    default: new Date()
  }
});

const Comment = mongoose.model("Comment", commentSchema);

export default Comment;