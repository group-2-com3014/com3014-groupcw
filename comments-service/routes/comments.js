import express from 'express';
import { getComments, getPostComments, deletePostComments, createComment, getCommentById, deleteCommentById } from '../controllers/comments.js';

const router = express.Router();

router.get("/", getComments);
router.get("/post/:postId", getPostComments);
router.delete("/post/:postId", deletePostComments);
router.post("/", createComment);
router.get("/:id", getCommentById);
router.delete("/:id", deleteCommentById);

export default router;