import request from "supertest";
import app from '../index.js';
import { connect, close, clear } from './db_setup.js';

process.env.NODE_ENV = "test";
const agent = request.agent(app);
const testPostId = 1;
const testCreatorId = 1;

beforeAll(async (done) => {
  await connect();
  done();
});

beforeEach(async (done) => {
  await clear();
  done();
});

afterAll(async (done) => {
  await close();
  done();
});

describe('get comments', () => {
  it('should return a 200 status', async (done) => {
    const res = await agent.get(`/comments`);
    expect(res.statusCode).toBe(200);

    done();
  });

  it('should return no comments', async (done) => {
    const res = await agent.get('/comments');
    expect(res.body.length).toEqual(0);

    done();
  });

  it('should return 2 comments', async (done) => {
    await agent.post('/comments').send({ body: "Test Comment 1", postId: testPostId, creatorId: testCreatorId });
    await agent.post('/comments').send({ body: "Test Comment 2", postId: testPostId, creatorId: testCreatorId });

    const res = await agent.get('/comments');
    expect(res.body.length).toEqual(2);

    done();
  });
});

describe('get comments by post', () => {
  it('should return status 200', async (done) => {
    const res = await agent.get(`/comments/post/1`);
    expect(res.statusCode).toBe(200);

    done();
  });

  it('should return 0 comments', async (done) => {
    const res = await agent.get(`/comments/post/1`);
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toEqual(0);

    done();
  });

  it('should return 1 comment', async (done) => {
    await agent.post('/comments').send({ body: "Test Comment 1", postId: testPostId, creatorId: testCreatorId });

    const res = await agent.get(`/comments/post/1`);
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toEqual(1);
    done();
  });
});

describe('create comment', () => {
  it('should successfully create comment', async (done) => {
    const res = await agent.post('/comments').send({ body: "Test Comment 1", postId: testPostId, creatorId: testCreatorId });
    expect(res.statusCode).toBe(201);
    expect(res.body.body).toEqual("Test Comment 1");
    expect(res.body.postId).toEqual(testPostId.toString());
    expect(res.body.creatorId).toEqual(testCreatorId.toString());

    done();
  });
});

describe('get comment by id', () => {
  it('should successfully get comment', async (done) => {
    const commentRes = await agent.post('/comments').send({ body: "Test Comment 1", postId: testPostId, creatorId: testCreatorId });
    const initialComment = commentRes.body;

    const res = await agent.get(`/comments/${initialComment._id}`);
    expect(res.statusCode).toBe(200);
    expect(res.body._id).toEqual(initialComment._id);
    expect(res.body.body).toEqual(initialComment.body);
    expect(res.body.postId).toEqual(initialComment.postId);
    expect(res.body.creatorId).toEqual(initialComment.creatorId);

    done();
  });

  it('should not find non existent comment', async (done) => {
    const res = await agent.get(`/comments/1`);
    expect(res.statusCode).toBe(500);

    done();
  });
});

describe('get comment by post id', () => {
  it('should successfully get comment', async (done) => {
    const commentRes = await agent.post('/comments').send({ body: "Test Comment 1", postId: testPostId, creatorId: testCreatorId });
    const initialComment = commentRes.body;

    const res = await agent.get(`/comments/post/${initialComment.postId}`);
    expect(res.statusCode).toBe(200);
    expect(res.body[0]._id).toEqual(initialComment._id);
    expect(res.body[0].body).toEqual(initialComment.body);
    expect(res.body[0].postId).toEqual(initialComment.postId);
    expect(res.body[0].creatorId).toEqual(initialComment.creatorId);

    done();
  });

  it('should find no comments', async (done) => {
    const res = await agent.get(`/comments/post/1`);
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toEqual(0);

    done();
  });
});

describe('delete comment by id', () => {
  it('should delete comment successfully', async (done) => {
    const commentRes = await agent.post('/comments').send({ body: "Test Comment 1", postId: testPostId, creatorId: testCreatorId });
    const initialComment = commentRes.body;

    const res = await agent.delete(`/comments/${initialComment._id}`);
    expect(res.statusCode).toBe(200);

    done();
  });

  it('should not delete non existent comment', async (done) => {
    const res = await agent.delete(`/comments/1`);
    expect(res.statusCode).toBe(404);

    done();
  });
});

describe('delete comment by post id', () => {
  it('should delete comment successfully', async (done) => {
    const commentRes = await agent.post('/comments').send({ body: "Test Comment 1", postId: testPostId, creatorId: testCreatorId });
    const initialComment = commentRes.body;
    
    const res = await agent.delete(`/comments/post/${initialComment.postId}`);
    expect(res.body.message).toEqual("Comments deleted successfully.");

    done();
  });
});