import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ApolloClient, ApolloProvider, InMemoryCache, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { darkTheme } from './styles.js'
import Home from './components/Pages/Home/Home.js';
import Header from './components/Header/Header.js';
import { Container, CssBaseline, ThemeProvider } from '@material-ui/core';
import ViewPost from './components/Pages/ViewPost/ViewPost.js';
import AddPost from './components/Pages/AddPost/AddPost.js';
import Auth from './components/Auth/Auth.js';
import { AuthProvider } from './context/auth.js';
import AuthRoute from './components/AuthRoute/AuthRoute.js';

const httpLink = createHttpLink({
  uri: 'http://localhost:5000/graphql'
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('profile');
  return {
    headers: {
      ...headers,
      Authorization: token ? `Bearer ${token}` : "",
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});

const App = () => {
  return (
    <BrowserRouter>
      <ApolloProvider client={client}>
        <AuthProvider>
          <ThemeProvider theme={darkTheme}>
            <CssBaseline />
            <Container maxWidth="lg">
              <Header />
              <Switch>
                <Route path="/" exact component={Home} />
                <AuthRoute path="/auth" component={Auth} />
                <Route path="/post/:id" component={ViewPost} />
                <Route path="/addPost" component={AddPost} />
              </Switch>
            </Container>
          </ThemeProvider>
        </AuthProvider>
      </ApolloProvider>
    </BrowserRouter>
  )
}

export default App
