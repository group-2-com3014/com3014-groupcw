import React, { Fragment, useContext, useState } from "react";
import { AuthContext } from '../../context/auth.js';
import { Divider, Typography, Grid, Button } from "@material-ui/core";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import useStyles from './styles.js';
import { useMutation } from "@apollo/client";
import { DELETE_COMMENT, GET_POST_BY_ID } from "../../queries/queries.js";

const Comment = ({ comment }) => {
  const classes = useStyles();
  const { user } = useContext(AuthContext);
  const [open, setOpen] = useState(false);

  const [deleteComment] = useMutation(DELETE_COMMENT, { 
    errorPolicy: "all",
    update() {
      setOpen(false);
    }
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDelete = () => {
    deleteComment({ 
      variables: { 
        commentId: comment._id, postId: comment.postId
      },
      refetchQueries: [{ query: GET_POST_BY_ID, variables: { postId: comment.postId } }]
    });
  }
  
  return (
    <Fragment>
      <Grid container wrap="nowrap" spacing={2}>
        <Grid item xs zeroMinWidth>
          <Typography variant="body1" className={classes.commentBody}>
            {`${comment.body} `}
          </Typography>
          <Typography variant="body2" className={classes.commentDate}>
            posted on {new Date(comment.createdAt).toDateString()} by {comment.creator.name}
          </Typography>
          { user && user._id === comment.creator._id && <Button variant="outlined" onClick={handleClickOpen}>Delete</Button>}
        </Grid>
      </Grid>
      <Divider variant="fullWidth" className={classes.commentDivider} />
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">Are you sure you want to delete this comment?</DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
};

export default Comment;