import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  commentCreator: { 
    margin: 0, 
    textAlign: "left" 
  },
  commentBody: {
    textAlign: "left"
  },
  commentDate: { 
    marginTop: "8px", 
    marginBottom: "16px", 
    textAlign: "left", 
    color: "gray" 
  },
  commentDivider: { 
    margin: "30px 0" 
  }
}));