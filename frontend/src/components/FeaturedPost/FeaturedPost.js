import React from 'react';
import { useHistory } from 'react-router';

import useStyles from './styles.js';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';

const FeaturedPost = ({ post }) => {
  const classes = useStyles();
  const history = useHistory();

  const viewPost = (postId) => {
    history.push(`/post/${postId}`);
  }
  
  return (
    <Grid item xs={12} md={6}>
      <CardActionArea component="a" onClick={() => viewPost(post._id)}>
        <Card className={classes.card}>
          <div className={classes.cardDetails}>
            <CardContent>
              <Typography component="h2" variant="h5">
                {post.title}
              </Typography>
              <Typography variant="subtitle1" color="textSecondary">
                {new Date(post.createdAt).toDateString()}
              </Typography>
              <Typography variant="subtitle1" paragraph>
                {post.description.length <= 100 ? post.description : `${post.description.substring(0,100)}...`}
              </Typography>
              <Typography variant="subtitle1" color="primary">
                Continue reading...
              </Typography>
            </CardContent>
          </div>
          <Hidden xsDown>
            <CardMedia className={classes.cardMedia} image={post.image} />
          </Hidden>
        </Card>
      </CardActionArea>
    </Grid>
  );
}

export default FeaturedPost;