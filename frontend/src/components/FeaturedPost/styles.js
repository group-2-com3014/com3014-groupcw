import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  card: {
    display: 'flex',
    minHeight: 200,
    maxHeight: 200
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
}));