import React, { useContext } from 'react';
import { useHistory } from 'react-router';
import { AuthContext } from '../../context/auth.js';
import useStyles from './styles.js';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const Header = () => {
  const classes = useStyles();
  const history = useHistory();
  const context = useContext(AuthContext);

  return (
    <React.Fragment>
      <Toolbar className={classes.toolbar}>
        <Typography component="h2" variant="h5" color="inherit" align="left" noWrap className={classes.toolbarTitle} onClick={() => history.push("/")}>
          JABS - Just Another Blog Site
        </Typography>
        {
          context.user && (<Button size="small" onClick={() => history.push("/addPost")} style={{ marginRight: "8px"}}>Add Post</Button>)
        }
        {
          context.user
          ? (<Button variant="outlined" size="small" onClick={context.signout}>Logout</Button>)
          : (<Button variant="outlined" size="small" onClick={() => history.push("/auth")}>Sign in</Button>)
        }
      </Toolbar>
    </React.Fragment>
  );
}

export default Header;