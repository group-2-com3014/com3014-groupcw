import React from 'react';
import { Grid, Paper, Typography } from '@material-ui/core';
import useStyles from './styles.js';

const Landing = ({ anyPosts }) => {
  const classes = useStyles();


  return (
    <Paper className={classes.landing}>
      <div className={classes.overlay} />
      <Grid container>
        <Grid item md={6}>
          <div className={classes.landingContent}>
            <Typography component="h1" variant="h3" color="inherit" gutterBottom>
              Welcome to our blog!
            </Typography>
            <Typography variant="h5" color="inherit" paragraph>
              { anyPosts 
              ? "To get started, you can view any of the posts available. To create and submit your own posts, please Login."
              : "It looks like there aren't any posts at the moment. Sign in and be the first to post!" }
            </Typography>
          </div>
        </Grid>
      </Grid>
    </Paper>
  )
}

export default Landing;
