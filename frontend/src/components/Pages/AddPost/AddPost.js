import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom'; 
import { useMutation } from '@apollo/client';
import { CREATE_POST, GET_POSTS } from '../../../queries/queries.js';
import { AuthContext } from '../../../context/auth.js'
import { Avatar, Button, Container, Paper, Grid, Typography, TextField } from '@material-ui/core';
import useStyles from './styles';
import PostAddIcon from '@material-ui/icons/PostAdd';
import { DropzoneArea } from 'material-ui-dropzone';

const AddPost = () => {
  const classes = useStyles();
  const history = useHistory();
  const { user } = useContext(AuthContext);

  const initialState = {
    title: "",
    description: "",
    image: "",
  };
  const [formData, setFormData] = useState(initialState);

  const validationState = {
    title: { error: false, errorMessage: ""},
    description: { error: false, errorMessage: ""},
  };
  const [errorData, setErrorData] = useState(validationState);

  const [createNewPost] = useMutation(CREATE_POST, {
    update(proxy, result) {
      const data = proxy.readQuery({
        query: GET_POSTS
      });
      if (data) {
        proxy.writeQuery({ query: GET_POSTS, data: { getPosts: [result.data.createPost, ...data.getPosts] }});
      }
      setFormData(initialState);
      history.push("/");
    },
    refetchQueries: [{ query: GET_POSTS }]
  });

  const validate = () => {
    let isValid = true;
    let startingState = {
      title: { error: false, errorMessage: ""},
      description: { error: false, errorMessage: ""},
    };

    if (formData.title.trim().length === 0) {
      startingState = { ...startingState, title: { error: true, errorMessage: "Required" } };
      isValid = false;
    }

    if (formData.description.trim().length === 0) {
      startingState = { ...startingState, description: { error: true, errorMessage: "Required" } };
      isValid = false;
    }

    setErrorData(startingState);
    return isValid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (validate()) {
      createNewPost({ 
        variables: {
          title: formData.title,
          description: formData.description,
          image: formData.image
        }
      })
    }
  };

  const base64conv = (files, cb) => {
    for (let i = 0; i < files.length; i++) {
      let file = files[i];
      
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        let fileInfo = {
          name: file.name,
          type: file.type,
          size: Math.round(file.size / 1000) + ' kB',
          base64: reader.result,
          file: file,
        };
        cb(fileInfo);
      }
    }
  };

  const handleFileUpload = async (images) => {
    if (images === undefined) return;

    base64conv(images, (fileInfo) => {
      setFormData({ ...formData, image: fileInfo.base64 })
    });
  };

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  return (
    <Container component="main" maxWidth="xs" style={{ marginBottom: "32px" }}>
      <Paper className={classes.paper} elevation={3}>
        {
          user ? 
          (
            <>
              <Avatar className={classes.avatar}>
                <PostAddIcon  />
              </Avatar>
              <Typography variant="h5">Add Post</Typography>
              <form className={classes.form} onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12}>
                    <TextField 
                      name="title"
                      onChange={handleChange}
                      variant="outlined"
                      required
                      fullWidth
                      error={errorData.title.error}
                      helperText={errorData.title.errorMessage}
                      label="Title"
                      autoFocus={true}
                      type="text"
                    />
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <TextField 
                      name="description"
                      onChange={handleChange}
                      variant="outlined"
                      required
                      fullWidth
                      error={errorData.description.error}
                      helperText={errorData.description.errorMessage}
                      label="Description"
                      multiline
                      rowsMax={4}
                    />
                  </Grid>
                  <DropzoneArea
                    acceptedFiles={['image/*']}
                    filesLimit={1}
                    dropzoneText={"Drag and drop an image here or click here"}
                    onChange={(files) => handleFileUpload(files)}
                  />
                </Grid>
                <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit} onClick={validate}>
                  Submit Post
                </Button>
              </form>
            </>
          ) 
          : <Typography variant="h5">Please Sign In</Typography>
        }
      </Paper>
    </Container>
  )
}

export default AddPost;
