import React from 'react';
import { useQuery } from '@apollo/client';
import { GET_POSTS } from '../../../queries/queries.js'
import PostsContainer from '../../PostsContainer/PostsContainer.js';
import Landing from '../../Landing/Landing.js';

const Home = () => {
  const { data, loading } = useQuery(GET_POSTS, { errorPolicy: 'all' });

  return (
    <React.Fragment>
      <main>
        <Landing anyPosts={data!== undefined && data?.getPosts.length !== 0}/>
        { !loading && data!== undefined && data?.getPosts.length !== 0 && <PostsContainer posts={data.getPosts} /> }
      </main>
    </React.Fragment>
  );
}

export default Home;