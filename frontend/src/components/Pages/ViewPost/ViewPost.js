import React, { useState, useContext } from 'react';
import { useQuery, useMutation } from '@apollo/client';
import { GET_POST_BY_ID, DELETE_POST, GET_POSTS, LIKE_POST, ADD_COMMENT } from '../../../queries/queries.js';
import { AuthContext } from '../../../context/auth.js'
import { Button, Card, CardContent, CardMedia, Divider, Grid, TextField, Typography } from '@material-ui/core';
import ThumbUpOutlinedIcon from '@material-ui/icons/ThumbUpOutlined';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useStyles from './styles.js';
import Comment from '../../Comment/Comment.js';
import { useParams, useHistory } from 'react-router';

const ViewPost = () => {
  const classes = useStyles();
  const history = useHistory();
  const { id } = useParams();
  const { data } = useQuery(GET_POST_BY_ID, { variables: { postId: id }}, { errorPolicy: "all" });
  const post = data?.getPost;
  const { user } = useContext(AuthContext);
  const[likePost] = useMutation(LIKE_POST, {
    errorPolicy: "all"
  });
  
  const [newComment, setNewComment] = useState("");
  const [commentErrors, setCommentErrors] = useState({ error: false, errorMessage: "" });
  const [open, setOpen] = useState(false);
  const [addComment] = useMutation(ADD_COMMENT, {
    errorPolicy: "all",
    update() {
      setNewComment("");
    }
  });

  const [deletePostAndComments] = useMutation(DELETE_POST, {
    errorPolicy: "all",
    update(proxy) {
      setOpen(false);
      const data = proxy.readQuery({ query: GET_POSTS });
      proxy.writeQuery({ query: GET_POSTS, data: { getPosts: data.getPosts.filter(p => p._id !== post._id) }})
      history.push("/");
    }
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  const handleDelete = () => {
    deletePost();
  }

  const deletePost = () => {
    deletePostAndComments({
      variables: { postId: post._id },
      refetchQueries: [{ query: GET_POSTS }],
    });
  }

  const validateComment = () => {
    let isValid = true;
    let validationState = { error: false, errorMessage: "" };

    if (newComment.trim().length === 0) {
      isValid = false;
      validationState = { error: true, errorMessage: "Comment must not be empty" };
    }
    setCommentErrors(validationState);

    return isValid;
  }

  const handleCommentSubmit = (e) => {
    e.preventDefault();
    
    if (validateComment()) {
      addComment({
        variables: {
          body: newComment,
          postId: post._id
        },
        refetchQueries: [{ query: GET_POST_BY_ID, variables: { postId: id } }]
      });
    }
  }

  const handleLikePost = () => {
    likePost({
      variables: { postId: post._id },
      refetchQueries: [{ query: GET_POST_BY_ID, variables: { postId: id } }]
    });
  }

  if (post === undefined) return "Loading Post...";
  
  return (
    <Card className={classes.postCard}>
      <CardContent>
        <Grid container>
          <Grid item xs={11}>
            <Typography variant="h4">{post.title}</Typography>
          </Grid>
          <Grid>
            { user && user._id === post.creator._id && <Button variant="outlined" onClick={() => handleClickOpen()}>Delete</Button>}
          </Grid>
        </Grid>
        <Divider />  
        <Typography variant="body1"><i>Posted by {post.creator.name} on {new Date(post.createdAt).toDateString()}</i></Typography>
          <CardMedia className={classes.postImage} image={post.image}/>
          <Typography variant="h6" className={classes.postDescription}>{post.description}</Typography>
          <Divider />
          <Grid container className={classes.addMarginTop}>
            <Grid item xs={10}>
              <Typography variant="h5">Comments ({post.comments.length})</Typography>
            </Grid>
            <Grid item align="center">
              <Typography variant="h5" >
                {post.likes.length} Like{post.likes.length > 1 && "s"}
              </Typography>
              <Button onClick={() => handleLikePost()} disabled={!user}>
                { user && post.likes.includes(user._id) 
                ? <ThumbUpIcon color="primary" />
                : <ThumbUpOutlinedIcon /> 
                }
              </Button>
            </Grid>
          </Grid>        
          { post.comments.map(comment => <Comment key={comment._id} comment={comment}/>) }
          { post.comments.length === 0 && <Divider /> }
          { user &&
            <Grid style={{ marginTop: "32px" }} container alignItems="center">
              <Grid item xs={10}>
                <TextField style={{ width: "100%"}} name="comment" label="Comment" variant="outlined" error={commentErrors.error} helperText={commentErrors.errorMessage} onChange={(e) => setNewComment(e.target.value)}/>
              </Grid>
              <Grid>
                <Button style={{ marginLeft: "16px" }} size="large" onClick={(e) => handleCommentSubmit(e)}>Add Comment</Button>
              </Grid>
            </Grid>
          }
      </CardContent>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Are you sure you want to delete this post?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            This post and all its comments and likes will be removed forever
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </Card>
  )
}

export default ViewPost;

