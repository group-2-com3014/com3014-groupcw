import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  postCard: {
    padding: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(4)
  },
  postImage: {
    paddingTop: '56.25%',
    marginTop: theme.spacing(4)
  },
  postDescription: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4)
  },
  addMarginTop: {
    marginTop: theme.spacing(4),
  }
}));