import React from 'react';
import { useHistory } from 'react-router';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { CardActionArea } from '@material-ui/core';
import useStyles from './styles.js';

const Post = ({ post }) => {
  const classes = useStyles();
  const history = useHistory();

  const viewPost = (postId) => {
    history.push(`/post/${postId}`);
  }

  return (
    <Grid item key={post._id} xs={12} sm={4} >
      <CardActionArea onClick={() => viewPost(post._id)}>
        <Card className={classes.card}>
          <CardMedia
            className={classes.cardMedia}
            image={post.image}
            title="Image title"
          />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">
              {post.title}
            </Typography>
            <Typography variant="subtitle1" color="textSecondary">
              Posted on {new Date(post.createdAt).toDateString()}
            </Typography>
            <Typography variant="subtitle1" paragraph>
              {post.description.length <= 45 ? post.description : `${post.description.substring(0,45)}...`}
            </Typography>
          </CardContent>
        </Card>
      </CardActionArea>
    </Grid>
  );
};

export default Post;
