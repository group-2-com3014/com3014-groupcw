import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  card: {
    height: '100%',
    minHeight: 350,
    maxHeight: 350,
    display: 'flex',
    flexDirection: 'column',
    "&:hover" : {
      cursor: "pointer"
    }
  },
  cardMedia: {
    paddingTop: '56.25%',
  },
  cardContent: {
    flexGrow: 1,
  }
}));