import React from 'react';
import Grid from '@material-ui/core/Grid';
import Post from './Post/Post.js';
import FeaturedPost from '../FeaturedPost/FeaturedPost.js';

const PostsContainer = ({ posts }) => {

  return (
    <Grid container spacing={4}>
      {
        posts.length <= 2 
        ? posts.map(post => <FeaturedPost key={post._id} post={post} />)
        : posts.slice(0, 2).map(post => <FeaturedPost key={post._id} post={post} />)
      }
      { posts.slice(2, posts.length).map(post => <Post key={post._id} post={post}/>) }
    </Grid>
  );
}

export default PostsContainer;