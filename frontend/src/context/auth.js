import React, { useReducer, createContext } from 'react';
import jwtDecode from 'jwt-decode';

const initialState = {
  user: null
};

if (localStorage.getItem('profile')) {
  const decodedToken = jwtDecode(localStorage.getItem('profile'));

  if (decodedToken.exp * 1000 < Date.now()) {
    localStorage.removeItem('profile');
  } else {
    initialState.user = decodedToken;
  }
}

const AuthContext = createContext({
  user: null,
  signin: (userData) => {},
  signout: () => {}
});

const authReducer = (state, action) => {
  switch (action.type) {
    case 'SIGNIN':
      return {
        ...state,
        user: action.payload
      };
    case 'SIGNOUT':
      return {
        ...state,
        user: null
      };
    default:
      return state;
  }
}

const AuthProvider = (props) => {
  const [state, dispatch] = useReducer(authReducer, initialState);

  const signin = (userData) => {
    localStorage.setItem('profile', userData.token);
    dispatch({
      type: 'SIGNIN',
      payload: userData
    });
  }

  const signout = () => {
    localStorage.removeItem('profile');
    dispatch({ type: 'SIGNOUT' });
  }

  return (
    <AuthContext.Provider
      value={{ user: state.user, signin, signout }}
      {...props}
    />
  );
}

export { AuthContext, AuthProvider };