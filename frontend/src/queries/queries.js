import { gql } from '@apollo/client';

export const GET_POSTS = gql`
{
  getPosts {
    _id
    title
    description
    creator {
      _id
      name
    }
    createdAt
    image
    likes
    comments {
      _id
      body
      creator {
        _id
        name
      }
      postId
    }
  }
}
`;

export const GET_POST_BY_ID = gql`
query($postId: ID!){
    getPost(postId: $postId) {
      _id
      title
      description
      creator {
        _id
        name
      }
      createdAt
      image
      likes
      comments {
        _id
        body
        createdAt
        creator {
          _id
          name
        }
        postId
      }
    }
 }
`;

export const CREATE_POST = gql`
mutation createPost($title: String!, $description: String!, $image: String!) {
  createPost(title: $title, description: $description, image: $image) {
    _id
    title
    description
    creator {
      _id
      name
    }
    createdAt
    image
    likes
    comments {
      _id
      body
      creator {
        _id
        name
      }
      postId
    }
  }
}
`;

export const SIGNUP = gql`
mutation signup($name: String! $email: String!, $password: String!) {
  signup(name: $name, email: $email, password: $password) {
    _id
    name
    email
    token
  }
}
`

export const SIGNIN = gql`
mutation signin($email: String!, $password: String!) {
  signin(email: $email, password: $password) {
    _id
    name
    email
    token
  }
}
`

export const DELETE_POST = gql`
mutation deletePostAndComments($postId: ID!) {
  deletePost(postId: $postId)
}
`

export const LIKE_POST = gql`
mutation likePost($postId: ID!) {
  likePost(postId: $postId) {
    _id
    title
    description
    creator {
      _id
      name
    }
    createdAt
    image
    likes
    comments {
      _id
      body
      creator {
        _id
        name
      }
      postId
    }
  }
}
`

export const ADD_COMMENT = gql`
  mutation createComment($body: String!, $postId: ID!) {
    createComment(body: $body, postId: $postId) {
      _id
      title
      description
      creator {
        _id
        name
      }
      createdAt
      image
      likes
      comments {
        _id
        body
        createdAt
        creator {
          _id
          name
        }
        postId
      }
    }
  }
`

export const DELETE_COMMENT = gql`
mutation deleteComment($commentId: ID!, $postId: ID!) {
  deleteComment(commentId: $commentId, postId: $postId) {
    _id
    title
    description
    creator {
      _id
      name
    }
    createdAt
    image
    likes
    comments {
      _id
      body
      createdAt
      creator {
        _id
        name
      }
      postId
    }
  }
}
`