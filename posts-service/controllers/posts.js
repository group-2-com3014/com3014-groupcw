import mongoose from 'mongoose';
import Post from '../models/Post.js';

export const getPosts = async (req, res) => {
  try {
    const posts = await Post.find().sort({ createdAt: "desc" });
    return res.status(200).json(posts);
  } catch (error) {
    return res.status(500).json({ message: "Something went wrong" });
  }
};

export const createPost = async (req, res) => {
  const post = new Post({ ...req.body, createdAt: new Date().toISOString() });

  try {
      await post.save();
      return res.status(201).json(post);
  } catch (error) {
      return res.status(409).json({ message: "Something went wrong" });
  }
}

export const getPostById = async (req, res) => {
  const { id } = req.params;

  try {
    const post = await Post.findById(id);
    return res.status(200).json(post);
  } catch (error) {
    return res.status(500).json({ message: "Something went wrong" });
  }
}

export const deletePost = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No post with id: ${id}`);

  await Post.findByIdAndRemove(id);
  return res.status(200).json({ message: "Post Deleted" });
}

export const likePost = async (req, res) => {
  const { id, userId } = req.params;
  const post = await Post.findById(id);

  if (post.likes.find((like) => like === userId)) {
    post.likes = post.likes.filter((like) => like !== userId);
  } else {
    post.likes.push(userId);
  }

  await post.save();
  return res.status(200).json(post);
}

export const addComment = async (req, res) => {
  const { id, commentId } = req.params;
  const post = await Post.findById(id);
  post.comments.push(commentId);
  await post.save();

  return res.status(200).json(post);
}

export const removeComment = async (req, res) => {
  const { id, commentId } = req.params;

  const post = await Post.findById(id);
  const commentIndex = post.comments.findIndex((c) => c === commentId);
  post.comments.splice(commentIndex, 1);
  await post.save();

  return res.status(200).json(post);
}