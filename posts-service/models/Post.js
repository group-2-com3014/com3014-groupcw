import mongoose from 'mongoose';

const postSchema = mongoose.Schema({
  title: String,
  description: String,
  creatorId: String,
  image: String,
  likes: {
    type: [String],
    default: []
  },
  createdAt: {
    type: Date,
    default: new Date()
  },
  comments: {
    type: [String],
    default: []
  }
});

const Post = mongoose.model("Post", postSchema);

export default Post;