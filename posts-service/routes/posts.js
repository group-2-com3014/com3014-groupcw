import express from 'express';
import { getPosts, createPost, getPostById, deletePost, likePost, addComment, removeComment } from '../controllers/posts.js';

const router = express.Router();

router.get("/", getPosts);
router.post("/", createPost);
router.get("/:id", getPostById);
router.delete("/:id", deletePost);
router.patch("/:id/like/:userId", likePost);
router.patch("/:id/comment/:commentId", addComment);
router.delete("/:id/comment/:commentId", removeComment);

export default router;