import request from "supertest";
import app from '../index.js';
import { connect, close, clear } from './db_setup.js';

process.env.NODE_ENV = "test";
const agent = request.agent(app);
const testUser = {
  name: "Test",
  email: "test@test.com",
  password: "test",
  id: 123
}

beforeAll(async (done) => {
  await connect();
  done();
});

beforeEach(async (done) => {
  await clear();
  done();
});

afterAll(async (done) => {
  await close();
  done();
});

describe('get all posts', () => {
  it('should return a 200', async (done) => {
    const res = await agent.get('/posts');
    expect(res.statusCode).toBe(200);
    
    done();
  });


  it('should return no posts', async (done) => {
    const res = await agent.get('/posts');
    expect(res.body.length).toEqual(0);

    done();
  });

  it('should return 2 posts', async (done) => {
    await agent.post('/posts').send({ title: "Test Title 1", description: "Test Desc 1", image: "Test Image 1", creatorId: testUser.id});
    await agent.post('/posts').send({ title: "Test Title 2", description: "Test Desc 2", image: "Test Image 2", creatorId: testUser.id});

    const res = await agent.get('/posts');
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toEqual(2);

    done();
  });
});

describe('get a post', () => {
  it('should return post', async (done) => {
    const createRes = await agent.post('/posts').send({ title: "Test Title 1", description: "Test Desc 1", image: "Test Image 1", creatorId: testUser.id});
    const post = createRes.body;

    const res = await agent.get(`/posts/${post._id}`);
    expect(res.statusCode).toBe(200);
    expect(res.body.title).toEqual(post.title);
    expect(res.body.description).toEqual(post.description);
    expect(res.body.image).toEqual(post.image);
    expect(res.body.creatorId).toEqual(post.creatorId);

    done();
  });

  it('should return error for non existent post', async (done) => {
    const res = await agent.get(`/posts/1`);
    expect(res.statusCode).toBe(500);

    done();
  });
});

describe('create a post', () => {
  it('should successfully create post', async (done) => {
    const res = await agent.post('/posts').send({ title: "Test Title 1", description: "Test Desc 1", image: "Test Image 1", creatorId: testUser.id});
    const post = res.body;
    expect(res.statusCode).toBe(201);
    expect(res.body.title).toEqual(post.title);
    expect(res.body.description).toEqual(post.description);
    expect(res.body.image).toEqual(post.image);
    expect(res.body.creatorId).toEqual(post.creatorId);

    done();
  })
});

describe('delete a post', () => {
  it('should successfully delete post', async (done) => {
    const createRes = await agent.post('/posts').send({ title: "Test Title 1", description: "Test Desc 1", image: "Test Image 1", creatorId: testUser.id});
    const post = createRes.body;

    const res = await agent.delete(`/posts/${post._id}`);
    expect(res.statusCode).toBe(200);
    done();
  });

  it('should return an error for non existent post', async (done) => {
    const res = await agent.delete(`/posts/1`);
    expect(res.statusCode).toBe(404);

    done();
  });
});

describe('like a post', () => {
  it('should successfully like post', async (done) => {
    const createRes = await agent.post('/posts').send({ title: "Test Title 1", description: "Test Desc 1", image: "Test Image 1", creatorId: testUser.id});
    const post = createRes.body;
    expect(post.likes.length).toEqual(0);

    const res = await agent.patch(`/posts/${post._id}/like/${testUser.id}`);
    const updatedPost = res.body;
    expect(res.statusCode).toBe(200);
    expect(updatedPost.likes.length).toEqual(1);

    done();
  });

  it('should successfully unlike post', async (done) => {
    const createRes = await agent.post('/posts').send({ title: "Test Title 1", description: "Test Desc 1", image: "Test Image 1", creatorId: testUser.id});
    const post = createRes.body;
    expect(post.likes.length).toEqual(0);

    await agent.patch(`/posts/${post._id}/like/${testUser.id}`);

    const res = await agent.patch(`/posts/${post._id}/like/${testUser.id}`);
    const updatedPost = res.body;
    expect(res.statusCode).toBe(200);
    expect(updatedPost.likes.length).toEqual(0);

    done();
  });
});

describe('add a comment to a post', () => {
  it('should successfully add comment', async (done) => {
    const testCommentId = 1;

    const createRes = await agent.post('/posts').send({ title: "Test Title 1", description: "Test Desc 1", image: "Test Image 1", creatorId: testUser.id});
    const post = createRes.body;
    expect(post.comments.length).toEqual(0);

    const res = await agent.patch(`/posts/${post._id}/comment/${testCommentId}`);
    const updatedPost = res.body;
    expect(updatedPost.comments.length).toEqual(1);

    done();
  });
});

describe('remove a comment on a post', () => {
  test('should successfully remove comment', async (done) => {
    const testCommentId = 1;

    const createRes = await agent.post('/posts').send({ title: "Test Title 1", description: "Test Desc 1", image: "Test Image 1", creatorId: testUser.id});
    const post = createRes.body;
    expect(post.comments.length).toEqual(0);

    const commentRes = await agent.patch(`/posts/${post._id}/comment/${testCommentId}`);
    const updatedPost = commentRes.body;
    expect(updatedPost.comments.length).toEqual(1);

    const res = await agent.delete(`/posts/${post._id}/comment/${testCommentId}`);
    const finalPost = res.body;
    expect(finalPost.comments.length).toEqual(0);

    done();
  })
});