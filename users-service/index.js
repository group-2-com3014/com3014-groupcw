import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';

import userRoutes from './routes/users.js';

dotenv.config();

const app = express();
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());
app.use("/user", userRoutes);

const PORT = 5001;

if (process.env.NODE_ENV !== "test") {
  mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => app.listen(PORT, "0.0.0.0", () => console.log(`Users service running on ${PORT}...`)))
  .catch((error) => console.log(error.message));

  mongoose.set("useFindAndModify", false);
}

export default app;