import express from 'express';

import { signin, signup, checkUserEmail, getUserById } from '../controllers/users.js';

const router = express.Router();

router.post('/signin', signin);
router.post('/signup', signup);
router.get('/user/check/:email', checkUserEmail);
router.get('/user/:id', getUserById);

export default router;