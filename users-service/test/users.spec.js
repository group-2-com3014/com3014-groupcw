import request from "supertest";
import bcrypt from 'bcryptjs';
import app from '../index.js';
import { connect, close, clear } from './db_setup.js';

process.env.NODE_ENV = "test";
process.env.SECRET_KEY = "test";
const agent = request.agent(app);

beforeAll(async (done) => {
  await connect();
  done();
});

beforeEach(async (done) => {
  await clear();
  done();
});

afterAll(async (done) => {
  await close();
  done();
});

describe('signup', () => {
  it('should sign up user successfully', async (done) => {
    const name = "Test User"
    const email = "test@test.com";
    const passwordPlain = "test123"
    const password = await bcrypt.hash(passwordPlain, 12);

    const res = await agent.post(`/user/signup`).send({ name, email, password });
    const user = res.body;
    expect(res.statusCode).toBe(200);
    expect(user.name).toEqual(name);
    expect(user.email).toEqual(email);
    expect(user._id).toBeDefined();
    
    done();
  });

  it('should not sign up existing email', async (done) => {
    const name = "Test User"
    const email = "test@test.com";
    const passwordPlain = "test123"
    const password = await bcrypt.hash(passwordPlain, 12);

    await agent.post(`/user/signup`).send({ name, email, password });

    const res = await agent.post(`/user/signup`).send({ name, email, password });
    expect(res.statusCode).toBe(400);
    
    done();
  });
});

describe('signin', () => {
  it('should successfully sign in', async (done) => {
    const name = "Test User"
    const email = "test@test.com";
    const passwordPlain = "test123"
    const password = await bcrypt.hash(passwordPlain, 12);

    await agent.post(`/user/signup`).send({ name, email, password });

    const res = await agent.post(`/user/signin`).send({ name, email, password: passwordPlain });
    expect(res.statusCode).toBe(200);
    
    done();
  });

  it('should not sign in non existent user', async (done) => {
    const email = "doesnotexist@test.com";
    const password = "test123";

    const res = await agent.post(`/user/signin`).send({ email, password });
    expect(res.statusCode).toBe(404);
    
    done();
  });

  it('should not sign in incorrect password', async (done) => {
    const name = "Test User"
    const email = "test@test.com";
    const passwordPlain = "test123"
    const password = await bcrypt.hash(passwordPlain, 12);

    await agent.post(`/user/signup`).send({ name, email, password });

    const res = await agent.post(`/user/signin`).send({ email, password: "INCORRECT_PASSWORD" });
    expect(res.statusCode).toBe(400);
    
    done();
  });
});

describe('get user by id', () => {
  it('should successfully get user by id', async (done) => {
    const name = "Test User"
    const email = "test@test.com";
    const passwordPlain = "test123"
    const password = await bcrypt.hash(passwordPlain, 12);

    const userRes = await agent.post(`/user/signup`).send({ name, email, password });
    const user = userRes.body;

    const res = await agent.get(`/user/user/${user._id}`);
    const userFound = res.body;
    expect(res.statusCode).toBe(200);
    expect(userFound.name).toEqual(user.name);
    expect(userFound.email).toEqual(user.email);

    done();
  });

  it('should not find non existent user', async (done) => {
    const res = await agent.get(`/user/user/1`);
    expect(res.statusCode).toBe(500);

    done();
  });
});

describe('get user by email', () => {
  it('should successfully get user by email', async (done) => {
    const name = "Test User"
    const email = "test@test.com";
    const passwordPlain = "test123"
    const password = await bcrypt.hash(passwordPlain, 12);

    const userRes = await agent.post(`/user/signup`).send({ name, email, password });
    const user = userRes.body;

    const res = await agent.get(`/user/user/check/${user.email}`);
    const userFound = res.body;
    expect(res.statusCode).toBe(200);
    expect(userFound.name).toEqual(user.name);
    expect(userFound.email).toEqual(user.email);

    done();
  });
});